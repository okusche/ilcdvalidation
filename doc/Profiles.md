Profiles are a means to perform certain validation procedures against a defined
set of rules which may differ from the standard ILCD XML Schema documents, 
validation style sheets, reference objects and/or categories. 

A profile is a set of XML Schema documents, validation style sheets, categories
documents and/or lists of reference objects, all bundled inside a JAR file. A 
manifest file inside the JAR provides all required meta information about the 
profile. 

Profiles are managed by the ProfileManager class. By default, the standard ILCD 
profile will be used for all validators without further explicit actions. To 
load a different profile, simply call the ProfileManager's `registerProfile()` 
method, supplying a URL to the profile JAR to be loaded as an argument. Once 
successfully loaded, the profile will then be available using the 
ProfileManager's `getProfiles()` method.    

By default, the standard ILCD profile will be used for all validators without 
further explicit actions or necessity to explicitly call the ProfileManager.

To load a different profile than the default one, use the ProfileManager's 
`registerProfile()` method. Once successfully loaded, the profile will then be 
available using the getProfiles() method and can be passed to a Validator.

The location of a profile JAR is provided as a URL. The JARs are cached in a 
directory which by default is a temporary one. This can be overridden by 
passing a location during initialization.

Within an environment like Eclipse, it might be necessary to use some custom 
resolution mechanism, which can be implemented in a custom Locator that can 
be passed as parameter during initialization.

The loading of default profiles can be controlled using during explicit
initialization using the ProfileManagerBuilder's
`registerDefaultProfiles(boolean registerDefaultProfile, boolean registerSecondaryDefaultProfiles)`
method as shown below, for example to prevent the ProfileManager from loading
any default profiles.

Explicit initialization has to occur before calling the ProfileManager's
`getInstance()` method for the first time. 

Example for passing parameters during initialization:

```java
    new ProfileManager.ProfileManagerBuilder()
        .cacheDir(File dir)
        .locator(new EclipseLocator())
        .registerDefaultProfiles(false, false)
        .build();
```

The EclipseLocator class could look like this:

```java
	public class EclipseLocator implements Locator {
		@Override
		public URL resolve(URL url) throws IOException {
			return FileLocator.resolve(url);
		}
	}
```

