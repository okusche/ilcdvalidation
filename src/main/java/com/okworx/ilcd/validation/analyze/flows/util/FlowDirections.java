package com.okworx.ilcd.validation.analyze.flows.util;

public enum FlowDirections {

    EMPTY, INPUTS_ONLY, OUTPUTS_ONLY, MIXED

}
