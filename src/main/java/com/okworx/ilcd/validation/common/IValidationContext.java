package com.okworx.ilcd.validation.common;

import java.util.Locale;

/**
 * <p>IValidationContext interface.</p>
 *
 * @author oliver.kusche
 * @version $Id: $Id
 */
public interface IValidationContext {

	/**
	 * <p>getLocale.</p>
	 *
	 * @return a {@link java.util.Locale} object.
	 */
	public Locale getLocale();

}
