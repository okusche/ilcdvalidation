package com.okworx.ilcd.validation.util;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.Logger;

/**
 * <p>ValidatorListener class.</p>
 *
 * @author oliver.kusche
 * @version $Id: $Id
 */
public class ValidatorListener implements ErrorListener {

	/** Constant <code>log</code> */
	protected static Logger log = org.apache.logging.log4j.LogManager.getLogger(ValidatorListener.class);

	private List<String> results = new ArrayList<String>();

	/**
	 * <p>Getter for the field <code>results</code>.</p>
	 *
	 * @return a {@link java.util.List} object.
	 */
	public List<String> getResults() {
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seejavax.xml.transform.ErrorListener#error(javax.xml.transform.
	 * TransformerException)
	 */
	/** {@inheritDoc} */
	public void error(TransformerException arg0) throws TransformerException {
		logEvent(arg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seejavax.xml.transform.ErrorListener#fatalError(javax.xml.transform.
	 * TransformerException)
	 */
	/** {@inheritDoc} */
	public void fatalError(TransformerException arg0) throws TransformerException {
		logEvent(arg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seejavax.xml.transform.ErrorListener#warning(javax.xml.transform.
	 * TransformerException)
	 */
	/** {@inheritDoc} */
	public void warning(TransformerException arg0) throws TransformerException {
		logEvent(arg0);
	}

	/**
	 * <p>logEvent.</p>
	 *
	 * @param arg0 a {@link javax.xml.transform.TransformerException} object.
	 * @throws javax.xml.transform.TransformerException if any.
	 */
	public void logEvent(TransformerException arg0) throws TransformerException {
		if (log.isDebugEnabled())
			log.debug(arg0.getMessage()); // arg0.getLocator().getLineNumber()
		if (arg0.getException() != null)
			results.add(arg0.getException().getMessage());
		else 
			results.add(arg0.getMessage());
	}

}
