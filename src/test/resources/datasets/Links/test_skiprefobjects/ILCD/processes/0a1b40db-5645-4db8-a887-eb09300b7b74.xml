<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet version="1.0" href="../../stylesheets/process2html.xsl" type="text/xsl"?>
<processDataSet xmlns="http://lca.jrc.it/ILCD/Process" xmlns:common="http://lca.jrc.it/ILCD/Common" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://lca.jrc.it/ILCD/Process ../../schemas/ILCD_ProcessDataSet.xsd" locations="../ILCDLocations.xml" version="1.1">
  <processInformation>
    <dataSetInformation>
      <common:UUID>0a1b40db-5645-4db8-a887-eb09300b7b74</common:UUID>
      <name>
        <baseName xml:lang="en">Electricity Mix</baseName>
        <treatmentStandardsRoutes xml:lang="en">AC</treatmentStandardsRoutes>
        <mixAndLocationTypes xml:lang="en">consumption mix, at consumer</mixAndLocationTypes>
        <functionalUnitFlowProperties xml:lang="en">1kV - 60kV</functionalUnitFlowProperties>
      </name>
      <common:synonyms xml:lang="en">power grid mix</common:synonyms>
      <complementingProcesses>
        <referenceToComplementingProcess type="process data set" refObjectId="0a1b40db-5645-4db8-a887-eb09300b7b75" uri="../processes/0a1b40db-5645-4db8-a887-eb09300b7b74.xml">
          <common:shortDescription xml:lang="en">Electricity Mix</common:shortDescription>
        </referenceToComplementingProcess>
      </complementingProcesses>
      <classificationInformation>
        <common:classification>
          <common:class level="0">Energy carriers and technologies</common:class>
          <common:class level="1">Electricity</common:class>
        </common:classification>
      </classificationInformation>
      <common:generalComment xml:lang="en">Good overall data quality. Energy carrier mix information based on official statistical information including import/export. Detailed
        power plant models were used, which combine measured emissions plus calculated values for not measured emissions of e.g. organics or heavy metals. Energy carrier extraction
        and processing data is of sufficient to good (e.g. refinery) data quality. Inventory is partly based on primary industry data, partly on secondary literature
        data.</common:generalComment>
    </dataSetInformation>
    <quantitativeReference type="Reference flow(s)">
      <referenceToReferenceFlow>63</referenceToReferenceFlow>
    </quantitativeReference>
    <time>
      <common:referenceYear>2002</common:referenceYear>
      <common:dataSetValidUntil>2010</common:dataSetValidUntil>
      <common:timeRepresentativenessDescription xml:lang="en">Annual average</common:timeRepresentativenessDescription>
    </time>
    <geography>
      <locationOfOperationSupplyOrProduction location="EU-27">
        <descriptionOfRestrictions xml:lang="en">The data set represents the country / region specific situation, focusing on the main technologies, the region specific
          characteristics and / or import statistics.</descriptionOfRestrictions>
      </locationOfOperationSupplyOrProduction>
    </geography>
    <technology>
      <technologyDescriptionAndIncludedProcesses xml:lang="en">The EU-27 specific power grid mix is shown in the pie chart "Power Grid Mix - EU-27". Each country provides a certain
        amount of electricity to the mix. In addition, the calculated breakdown of the energy carriers used for generating the electricity is shown below. The electricity is either
        produced in energy carrier specific power plants and / or energy carrier specific heat and power plants (CHP). For more details see the corresponding country specific data
        sets. Each country specific fuel supply (share of resources used, by import and / or domestic supply) including the country specific energy carrier properties (e.g. element
        and energy contents) are accounted for. Furthermore country specific technology standards of power plants regarding efficiency, firing technology, flue-gas
        desulphurisation, NOx removal and dedusting are considered. Each country specific electricity consumption mix is modelled as shown in the flow diagram "Modelling of Power
        Consumption Mix". It includes imported/exported electricity, distribution losses (in %) and the own use by energy producers. The data set considers the whole supply chain
        of the fuels from exploration over extraction and preparation to transport of fuels to the power plants.    The background system is addressed as follows:  Transports: All
        relevant and known transport processes used are included. Overseas transports including rail and truck transport to and from major ports for imported bulk resources are
        included. Furthermore all relevant and known pipeline and / or tanker transport of gases and oil imports are included.  Energy carriers: Coal, crude oil, natural gas and
        uranium are modelled according to the specific import situation.  Refinery products: Diesel, gasoline, technical gases, fuel oils, basic oils and residues such as bitumen
        are modelled via a country-specific, refinery parameterized model. The refinery model represents the current national standard in refinery techniques (e.g. emission level,
        internal energy consumption,...) as well as the individual country-specific product output spectrum, which can be quite different from country to country. Hence the
        refinery products used show the individual country-specific use of resources. The supply of crude oil is modelled, again, according to the country-specific crude oil
        situation with the respective properties of the resources.</technologyDescriptionAndIncludedProcesses>
      <technologicalApplicability xml:lang="en">Medium voltage (1kV - 60kV) electricity for final consumers.</technologicalApplicability>
      <referenceToTechnologyPictogramme refObjectId="5d9e826d-70cd-4539-98f1-e0f05f58747e" type="source data set" uri="../sources/5d9e826d-70cd-4539-98f1-e0f05f58747e.xml">
        <common:shortDescription xml:lang="en">PE_LBP-GaBi_Energy_Electricity_Country-Mix_5d9e826d-70cd-4539-98f1-e0f05f58747e.jpg</common:shortDescription>
      </referenceToTechnologyPictogramme>
      <referenceToTechnologyFlowDiagrammOrPicture refObjectId="3cb18489-4359-11dd-ae16-0800200c9a66" type="source data set"
        uri="../sources/3cb18489-4359-11dd-ae16-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">PE_LBP-GaBi_Energy_Electricity_Mix_EU-27_2_3cb18489-4359-11dd-ae16-0800200c9a66.jpg</common:shortDescription>
      </referenceToTechnologyFlowDiagrammOrPicture>
      <referenceToTechnologyFlowDiagrammOrPicture refObjectId="3cb18488-4359-11dd-ae16-0800200c9a66" type="source data set"
        uri="../sources/3cb18488-4359-11dd-ae16-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">PE_LBP-GaBi_Energy_Electricity_Mix_EU-27_1_3cb18488-4359-11dd-ae16-0800200c9a66.jpg</common:shortDescription>
      </referenceToTechnologyFlowDiagrammOrPicture>
      <referenceToTechnologyFlowDiagrammOrPicture refObjectId="1318cdbd-5fba-11db-b0de-0800200c9a66" type="source data set"
        uri="../sources/1318cdbd-5fba-11db-b0de-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">PE_LBP-GaBi_Energy_Electricity_Mix_Modelling_1318cdbd-5fba-11db-b0de-0800200c9a66.jpg</common:shortDescription>
      </referenceToTechnologyFlowDiagrammOrPicture>
    </technology>
  </processInformation>
  <modellingAndValidation>
    <LCIMethodAndAllocation>
      <typeOfDataSet>Unit process, single operation</typeOfDataSet>
      <LCIMethodPrinciple>Attributional</LCIMethodPrinciple>
      <deviationsFromLCIMethodPrinciple xml:lang="en">None</deviationsFromLCIMethodPrinciple>
      <LCIMethodApproaches>Allocation - exergetic content</LCIMethodApproaches>
      <LCIMethodApproaches>Allocation - net calorific value</LCIMethodApproaches>
      <LCIMethodApproaches>Allocation - market value</LCIMethodApproaches>
      <LCIMethodApproaches>Allocation - mass</LCIMethodApproaches>
      <deviationsFromLCIMethodApproaches>For the combined heat and power production, allocation by exergetic content is applied. For the electricity generation and by-products,
        e.g. gypsum, allocation by market value is applied due to no common physical properties. Within the refinery allocation by net calorific value and mass is used. For the
        combined crude oil, natural gas and natural gas liquids production allocation by net calorific value is applied.</deviationsFromLCIMethodApproaches>
      <modellingConstants xml:lang="en">All data used in the calculation of the LCI results refer to net calorific value. For the transport of energy carriers average transport
        processes with specific parameter settings e.g. transport distances and route (ship, pipeline, rail, road) are used.</modellingConstants>
      <deviationsFromModellingConstants xml:lang="en">None</deviationsFromModellingConstants>
      <referenceToLCAMethodDetails refObjectId="10466572-0cfd-428e-8b10-ae74255e6e83" type="source data set" uri="../sources/10466572-0cfd-428e-8b10-ae74255e6e83.xml">
        <common:shortDescription xml:lang="en">GaBi Modelling Principles</common:shortDescription>
      </referenceToLCAMethodDetails>
    </LCIMethodAndAllocation>
    <dataSourcesTreatmentAndRepresentativeness>
      <dataCutOffAndCompletenessPrinciples xml:lang="en">Cut-off rules for each unit process: Coverage of at least 95 % of mass and energy of the input and output flows, and 98 %
        of their environmental relevance (according to expert judgement).</dataCutOffAndCompletenessPrinciples>
      <deviationsFromCutOffAndCompletenessPrinciples xml:lang="en">The coverage for the exploration data (crude oil, natural gas, natural gas liquids) is only 90% of mass and
        energy and 95% of the environmental relevance (expert judgement).</deviationsFromCutOffAndCompletenessPrinciples>
      <dataSelectionAndCombinationPrinciples xml:lang="en">The data sources for the complete product system are sufficiently consistent: The grid mix data is based on national
        statistics. The key emissions e.g. sulphur dioxide, nitrogen oxide, etc., of the power plants are based on measured operating data taken from national statistics. All other
        emissions from the power plants are based on literature data and / or calculated via energy carrier composition in combination with (literature-based) combustion models.
        Detailed power plant models were used, which combine measured emissions plus calculated values for not measured emissions of e.g. organics or heavy metals. Infrastructure
        data are from literature. The data on the energy carrier supply chain is based on statistics with country / region-specific transport distances and energy carrier
        composition, as well as industry and literature data on the inventory of exploration and extraction. Refinery data are also based on statistical data and measurements of
        major refineries as well as literature data. LCI modelling is fully consistent.</dataSelectionAndCombinationPrinciples>
      <deviationsFromSelectionAndCombinationPrinciples xml:lang="en">None</deviationsFromSelectionAndCombinationPrinciples>
      <dataTreatmentAndExtrapolationsPrinciples xml:lang="en">Energy carrier specific power plants are modelled according to national combustion technology mix. Data measured at
        representative power plants and published have been used to represent the country / region mix of power plant technologies.</dataTreatmentAndExtrapolationsPrinciples>
      <deviationsFromTreatmentAndExtrapolationPrinciples xml:lang="en">None</deviationsFromTreatmentAndExtrapolationPrinciples>
      <referenceToDataSource refObjectId="d2740c8a-5fbd-11db-b0de-0800200c9a66" type="source data set" uri="../sources/d2740c8a-5fbd-11db-b0de-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">IEA Statistics - Electricity information 2004, 2004</common:shortDescription>
      </referenceToDataSource>
      <referenceToDataSource refObjectId="d2740c8b-5fbd-11db-b0de-0800200c9a66" type="source data set" uri="../sources/d2740c8b-5fbd-11db-b0de-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">IEA Statistics - Oil Information 2004, 2004</common:shortDescription>
      </referenceToDataSource>
      <percentageSupplyOrProductionCovered>95.0</percentageSupplyOrProductionCovered>
      <useAdviceForDataSet xml:lang="en">Use by medium voltage electricity customers without own electricity generators or transformers (e.g. at industry and SME), which use
        electricity directly from the grid. The data set can be used for all LCI/LCA studies where electricity is needed.</useAdviceForDataSet>
    </dataSourcesTreatmentAndRepresentativeness>
    <completeness>
      <completenessProductModel>All relevant flows quantified</completenessProductModel>
      <referenceToSupportedImpactAssessmentMethods type="LCIA method data set" refObjectId="8b82be1e-c0ce-458f-a55f-113aa51b743a" uri="../lciamethods/8b82be1e-c0ce-458f-a55f-113aa51b743a.xml">
        <common:shortDescription xml:lang="en">Super LCIA 2007</common:shortDescription>
      </referenceToSupportedImpactAssessmentMethods>
    </completeness>
    <validation>
      <review type="Dependent internal review">
        <common:scope name="Raw data">
          <common:method name="Cross-check with other source"/>
          <common:method name="Expert judgement"/>
        </common:scope>
        <common:scope name="Unit process(es), black box">
          <common:method name="Energy balance"/>
          <common:method name="Element balance"/>
          <common:method name="Cross-check with other source"/>
          <common:method name="Cross-check with other data set"/>
          <common:method name="Expert judgement"/>
          <common:method name="Mass balance"/>
          <common:method name="Compliance with ISO 14040 to 14044"/>
        </common:scope>
        <common:scope name="LCI results or Partly terminated system">
          <common:method name="Energy balance"/>
          <common:method name="Cross-check with other data set"/>
          <common:method name="Expert judgement"/>
          <common:method name="Mass balance"/>
          <common:method name="Compliance with ISO 14040 to 14044"/>
        </common:scope>
        <common:scope name="LCIA results">
          <common:method name="Cross-check with other source"/>
          <common:method name="Cross-check with other data set"/>
          <common:method name="Expert judgement"/>
        </common:scope>
        <common:scope name="Documentation">
          <common:method name="Expert judgement"/>
          <common:method name="Compliance with ISO 14040 to 14044"/>
        </common:scope>
        <common:scope name="Life cycle inventory methods">
          <common:method name="Compliance with ISO 14040 to 14044"/>
        </common:scope>
        <common:reviewDetails xml:lang="en">Inventory: The internal review was done by several iteration steps concerning raw data validation, raw data documentation,
          representativity, completeness and consistency of modelling with regard to ISO 14040 and 14044.; Documentation: The review of the documentation was performed by Ecobilan
          and is in compliance with ISO 14040 and 14044. The data set documentation is correct in view of the appropriateness of the information provided. It includes all relevant
          information in view of data quality and scope of application of the respective LCI result.</common:reviewDetails>
        <common:referenceToNameOfReviewerAndInstitution refObjectId="623edf96-39d1-4e6f-9892-674c7228546b" type="contact data set"
          uri="../contacts/623edf96-39d1-4e6f-9892-674c7228546b.xml">
          <common:shortDescription xml:lang="en">PE INTERNATIONAL</common:shortDescription>
        </common:referenceToNameOfReviewerAndInstitution>
        <common:referenceToNameOfReviewerAndInstitution refObjectId="de53c560-d6a9-11da-a94d-0800200c9a66" type="contact data set"
          uri="../contacts/de53c560-d6a9-11da-a94d-0800200c9a66.xml">
          <common:shortDescription xml:lang="en">LBP-GaBi</common:shortDescription>
        </common:referenceToNameOfReviewerAndInstitution>
        <common:referenceToNameOfReviewerAndInstitution refObjectId="d571097d-d600-11da-a94d-0800200c9a66" type="contact data set"
          uri="../contacts/d571097d-d600-11da-a94d-0800200c9a66.xml">
          <common:shortDescription xml:lang="en">Ecobilan</common:shortDescription>
        </common:referenceToNameOfReviewerAndInstitution>
      </review>
    </validation>
    <complianceDeclarations>
      <compliance>
        <common:referenceToComplianceSystem type="source data set" uri="../sources/d92a1a12-2545-49e2-a585-55c259997756.xml" refObjectId="d92a1a12-2545-49e2-a585-55c259997756">
          <common:shortDescription>ILCD Data Network - Entry-level</common:shortDescription>
        </common:referenceToComplianceSystem>
        <common:approvalOfOverallCompliance>Not compliant</common:approvalOfOverallCompliance>
        <common:nomenclatureCompliance>Fully compliant</common:nomenclatureCompliance>
        <common:methodologicalCompliance>Fully compliant</common:methodologicalCompliance>
        <common:reviewCompliance>Not compliant</common:reviewCompliance>
        <common:documentationCompliance>Not compliant</common:documentationCompliance>
      </compliance>
    </complianceDeclarations>
  </modellingAndValidation>
  <administrativeInformation>
    <common:commissionerAndGoal>
      <common:referenceToCommissioner refObjectId="623edf96-39d1-4e6f-9892-674c7228546b" type="contact data set" uri="../contacts/623edf96-39d1-4e6f-9892-674c7228546b.xml">
        <common:shortDescription xml:lang="en">PE INTERNATIONAL</common:shortDescription>
      </common:referenceToCommissioner>
      <common:referenceToCommissioner refObjectId="5bb337b0-9a1a-11da-a72b-0800200c9a62" type="contact data set" uri="../contacts/5bb337b0-9a1a-11da-a72b-0800200c9a62.xml">
        <common:shortDescription xml:lang="en">EC, DG ENV</common:shortDescription>
      </common:referenceToCommissioner>
      <common:referenceToCommissioner refObjectId="d0d5f8bb-9311-49d1-9e30-2f20a6977f4f" type="contact data set" uri="../contacts/d0d5f8bb-9311-49d1-9e30-2f20a6977f4f.xml">
        <common:shortDescription xml:lang="en">European Commission, JRC-IES</common:shortDescription>
      </common:referenceToCommissioner>
      <intendedApplications xmlns="http://lca.jrc.it/ILCD/Common" xml:lang="en">This background LCI data set can be used for any types of LCA studies.</intendedApplications>
    </common:commissionerAndGoal>
    <dataGenerator>
      <common:referenceToPersonOrEntityGeneratingTheDataSet refObjectId="623edf96-39d1-4e6f-9892-674c7228546b" type="contact data set"
        uri="../contacts/623edf96-39d1-4e6f-9892-674c7228546b.xml">
        <common:shortDescription xml:lang="en">PE INTERNATIONAL</common:shortDescription>
      </common:referenceToPersonOrEntityGeneratingTheDataSet>
      <common:referenceToPersonOrEntityGeneratingTheDataSet refObjectId="de53c560-d6a9-11da-a94d-0800200c9a66" type="contact data set"
        uri="../contacts/de53c560-d6a9-11da-a94d-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">LBP-GaBi</common:shortDescription>
      </common:referenceToPersonOrEntityGeneratingTheDataSet>
    </dataGenerator>
    <dataEntryBy>
      <common:timeStamp>2012-01-20T10:26:17+01:00</common:timeStamp>
      <common:referenceToDataSetFormat type="source data set" uri="../sources/a97a0155-0234-4b87-b4ce-a45da52f2a40.xml" refObjectId="a97a0155-0234-4b87-b4ce-a45da52f2a40">
        <common:shortDescription>ILCD format</common:shortDescription>
      </common:referenceToDataSetFormat>
      <common:referenceToConvertedOriginalDataSetFrom refObjectId="6df7ba00-86d1-11db-b606-0800200c9a66" type="source data set"
        uri="../sources/6df7ba00-86d1-11db-b606-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">GaBi databases 2006</common:shortDescription>
      </common:referenceToConvertedOriginalDataSetFrom>
      <common:referenceToPersonOrEntityEnteringTheData refObjectId="de53c560-d6a9-11da-a94d-0800200c9a66" type="contact data set"
        uri="../contacts/de53c560-d6a9-11da-a94d-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">LBP-GaBi</common:shortDescription>
      </common:referenceToPersonOrEntityEnteringTheData>
      <common:referenceToDataSetUseApproval refObjectId="be34bbb0-b054-11db-abbd-0800200c9a66" type="contact data set" uri="../contacts/be34bbb0-b054-11db-abbd-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">No official approval by producer or operator</common:shortDescription>
      </common:referenceToDataSetUseApproval>
    </dataEntryBy>
    <publicationAndOwnership>
      <common:dataSetVersion>03.00.000</common:dataSetVersion>
      <common:referenceToPrecedingDataSetVersion type="process data set" refObjectId="0a1b40db-5645-4db8-a887-eb09300b7b74"
        uri="http://lca.jrc.ec.europa.eu/lcainfohub/datasets/elcd/processes/0a1b40db-5645-4db8-a887-eb09300b7b74.xml">
        <common:shortDescription xml:lang="en">Electricity Mix; AC; consumption mix, at consumer; 1kV - 60kV</common:shortDescription>
      </common:referenceToPrecedingDataSetVersion>
      <common:permanentDataSetURI>http://lca.jrc.ec.europa.eu/lcainfohub/datasets/elcd/processes/0a1b40db-5645-4db8-a887-eb09300b7b74.xml</common:permanentDataSetURI>
      <common:workflowAndPublicationStatus>Data set finalised; entirely published</common:workflowAndPublicationStatus>
      <common:referenceToUnchangedRepublication refObjectId="57bbc510-79c3-11dd-ad8b-0800200c9a66" type="source data set" uri="../sources/57bbc510-79c3-11dd-ad8b-0800200c9a66.xml">
        <common:shortDescription xml:lang="en">ELCD database 2.0</common:shortDescription>
      </common:referenceToUnchangedRepublication>
      <common:referenceToOwnershipOfDataSet refObjectId="623edf96-39d1-4e6f-9892-674c7228546b" type="contact data set" uri="../contacts/623edf96-39d1-4e6f-9892-674c7228546b.xml">
        <common:shortDescription xml:lang="en">PE INTERNATIONAL</common:shortDescription>
      </common:referenceToOwnershipOfDataSet>
      <common:copyright>true</common:copyright>
      <common:accessRestrictions xml:lang="en">The data set can be used free of charge by anybody to perform LCA studies, to distribute it to third parties, to convert it to other
        formats, to develop own data sets etc. as long as the copyright and license conditions for the ELCD data sets and the ILCD format are met that can be accessed via
        http://lca.jrc.ec.europa.eu. Please note e.g. that reference must be given to the 'Owner of data set' and to the 'ELCD database' plus version number, when using the data
        set or parts thereof. Please note also, that any modifications/omissions of the data set results in invalidity of any existing 'Official approval of data set by
        producer/operator', that the impression must be avoided that this would still be a complete ELCD data set, and that the content of further fields has to be adjusted. For
        details see the aforementioned copyright and license conditions.</common:accessRestrictions>
    </publicationAndOwnership>
  </administrativeInformation>
  <exchanges>
    <exchange dataSetInternalID="0">
      <referenceToFlowDataSet refObjectId="fe0acd60-3ddc-11dd-aaa4-0050c2490048" type="flow data set" uri="../flows/fe0acd60-3ddc-11dd-aaa4-0050c2490048.xml">
        <common:shortDescription xml:lang="en">air (Resources from air)</common:shortDescription>
      </referenceToFlowDataSet>
      <exchangeDirection>Input</exchangeDirection>
      <meanAmount>2.74441901235726</meanAmount>
      <resultingAmount>2.74441901235726</resultingAmount>
      <dataSourceType>Mixed primary / secondary</dataSourceType>
      <dataDerivationTypeStatus>Unknown derivation</dataDerivationTypeStatus>
    </exchange>
    <exchange dataSetInternalID="1">
      <referenceToFlowDataSet refObjectId="08a91e70-3ddc-11dd-97dc-0050c2490048" type="flow data set" uri="../flows/08a91e70-3ddc-11dd-97dc-0050c2490048.xml">
        <common:shortDescription xml:lang="en">barium sulfate (Resources from ground)</common:shortDescription>
      </referenceToFlowDataSet>
      <exchangeDirection>Input</exchangeDirection>
      <meanAmount>5.53614291202287E-14</meanAmount>
      <resultingAmount>5.53614291202287E-14</resultingAmount>
      <dataSourceType>Mixed primary / secondary</dataSourceType>
      <dataDerivationTypeStatus>Unknown derivation</dataDerivationTypeStatus>
    </exchange>
    <exchange dataSetInternalID="2">
      <referenceToFlowDataSet refObjectId="08a91e70-3ddc-11dd-97f9-0050c2490048" type="flow data set" uri="../flows/08a91e70-3ddc-11dd-97f9-0050c2490048.xml">
        <common:shortDescription xml:lang="en">baryte (Resources from ground)</common:shortDescription>
      </referenceToFlowDataSet>
      <exchangeDirection>Input</exchangeDirection>
      <meanAmount>0.000190967238617485</meanAmount>
      <resultingAmount>0.000190967238617485</resultingAmount>
      <dataSourceType>Mixed primary / secondary</dataSourceType>
      <dataDerivationTypeStatus>Unknown derivation</dataDerivationTypeStatus>
    </exchange>
    <exchange dataSetInternalID="3">
      <referenceToFlowDataSet refObjectId="4d9a8790-3ddd-11dd-978b-0050c2490048" type="flow data set" uri="../flows/4d9a8790-3ddd-11dd-978b-0050c2490048.xml">
        <common:shortDescription xml:lang="en">basalt (Resources from ground)</common:shortDescription>
      </referenceToFlowDataSet>
      <exchangeDirection>Input</exchangeDirection>
      <meanAmount>0.000651728167229204</meanAmount>
      <resultingAmount>0.000651728167229204</resultingAmount>
      <dataSourceType>Mixed primary / secondary</dataSourceType>
      <dataDerivationTypeStatus>Unknown derivation</dataDerivationTypeStatus>
    </exchange>
    <exchange dataSetInternalID="4">
      <referenceToFlowDataSet refObjectId="08a91e70-3ddc-11dd-97dd-0050c2490048" type="flow data set" uri="../flows/08a91e70-3ddc-11dd-97dd-0050c2490048.xml">
        <common:shortDescription xml:lang="en">bauxite (Resources from ground)</common:shortDescription>
      </referenceToFlowDataSet>
      <exchangeDirection>Input</exchangeDirection>
      <meanAmount>3.92276840345205E-6</meanAmount>
      <resultingAmount>3.92276840345205E-6</resultingAmount>
      <dataSourceType>Mixed primary / secondary</dataSourceType>
      <dataDerivationTypeStatus>Unknown derivation</dataDerivationTypeStatus>
    </exchange>
  </exchanges>
</processDataSet>
