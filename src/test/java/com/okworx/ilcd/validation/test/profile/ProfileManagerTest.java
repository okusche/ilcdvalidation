package com.okworx.ilcd.validation.test.profile;

import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.okworx.ilcd.validation.exception.InvalidProfileException;
import com.okworx.ilcd.validation.profile.Profile;
import com.okworx.ilcd.validation.profile.ProfileManager;
import com.okworx.ilcd.validation.test.TestConfig;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProfileManagerTest {

	protected final Logger log = org.apache.logging.log4j.LogManager.getLogger(this.getClass());

	@Before
	public void reset() {
		ProfileManager.getInstance().reset(true);
	}

	@Test
	public void testLoadProfile() throws IOException {

		ProfileManager pm = ProfileManager.getInstance();
		log.info(pm.getDefaultProfile().getResourceAsStream("eu/europa/ec/jrc/lca/ilcd/schemas/ext/xml.xsd")
				.available());

	}

	@Test
	public void testLoadDefaultProfile() {
		ProfileManager.getInstance();
	}

	@Test
	public void testLoadEPDProfile() throws InvalidProfileException {

		ProfileManager pm = ProfileManager.getInstance();
		pm.registerProfile(TestConfig.getEPDProfileURL());

	}

	@Test
	public void testProfileHasVersion() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		assertEquals(pm.getProfile(TestConfig.getEFProfileURL()).getVersion(), TestConfig.EF_PROFILE_VERSION);
	}

	@Test
	public void testProfileIsComposed() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		assertFalse(pm.getProfile(TestConfig.getELProfileURL()).isComposed());
		assertTrue(pm.getProfile(TestConfig.getSchemaTestProfileURL()).isComposed());
	}

	@Test
	public void schemaBundlesExists() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		Profile.Bundle<String[]>[] paths = pm.getProfile(TestConfig.getSchemaTestProfileURL()).getSchemaBundles();
		assertEquals(paths.length, 2);
		assertEquals(paths[0].getResource().length, 1);
		assertEquals(paths[1].getResource().length, 7);
	}

	@Test
	public void stylesheetBundlesExists() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		Profile.Bundle<String>[] paths = pm.getProfile(TestConfig.getAddStylesheetTestProfileURL()).getStylesheetBundles();
		assertEquals(paths.length, 2);
		assertNotNull(paths[0].getResource());
		assertNotNull(paths[1].getResource());
	}

	@Test
	public void correctCategoriesFile() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		Profile.Bundle<String> categoriesFile = pm.getProfile(TestConfig.getCategoriesTestProfileURL()).getCategoriesBundle();
		assertEquals(categoriesFile.getResource(), "com/okworx/test/add_categories/categories/CPC_2_1.xml");
	}

	@Test
	public void hasChangelog() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		assertNotNull(pm.getProfile(TestConfig.getEFProfileURL()).getSemanticChangelog());
		assertNotNull(pm.getProfile(TestConfig.getEFProfileURL()).getTechnicalChangelog());
	}

	@Test
	public void overridesElementaryFlow() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		Profile.Bundle<String> elFlow = pm.getProfile(TestConfig.getRefObjectsTestProfileURL()).getReferenceElementaryFlows();
		assertTrue(elFlow.getJarPath().contains("refobjects"));
		assertEquals(elFlow.getUrlPrefix(), "com/okworx/test/reference");
		assertEquals(elFlow.getResource(), "EPD_elementary_flows_03a_2019.ser.z");
	}

	@Test
	public void addRefObjects() throws IOException, InvalidProfileException {
		ProfileManager pm = ProfileManager.getInstance();
		Profile.Bundle<String>[] bundles = pm.getProfile(TestConfig.getRefObjectsTestProfileURL()).getReferenceObjectsOther();

		assertEquals(bundles.length, 2);
		assertTrue(bundles[0].getJarPath().contains("refobjects"));
		assertEquals(bundles[0].getUrlPrefix(), "com/okworx/test/reference");
		assertEquals(bundles[0].getResource(), "EPD_reference_objects_03a_2019.ser.z");
		assertTrue(bundles[1].getJarPath().contains("test-base"));
		assertEquals(bundles[1].getUrlPrefix(), "com/okworx/test");
		assertEquals(bundles[1].getResource(), "EF_reference_objects_rev3.0.ser.z");
	}
}
